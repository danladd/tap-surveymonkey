"""Tests standard tap features using the built-in SDK tests library."""

import json
from pathlib import Path

from singer_sdk.testing import get_standard_tap_tests

from tap_surveymonkey.tap import TapSurveyMonkey


SAMPLE_CONFIG = json.loads(Path("./.secrets/config.json").read_text())


# Run standard built-in tap tests from the SDK:
def test_standard_tap_tests():
    """Run standard tap tests from the SDK."""
    tests = get_standard_tap_tests(
        TapSurveyMonkey,
        config=SAMPLE_CONFIG
    )
    for test in tests:
        test()

