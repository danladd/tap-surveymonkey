"""Stream type classes for tap-surveymonkey."""

from pathlib import Path
from typing import Any, Dict, Optional
import copy

from singer_sdk import typing as th
from tap_surveymonkey.client import SurveyMonkeyStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")

class SurveysStream(SurveyMonkeyStream):
    """This Stream is only necessary as a parent to list all the surveys.
    You can ignore its output as its duplicated SurveyDetailsStream"""
    name = "surveys"
    path = f"/surveys"
    primary_keys = ["id"]
    records_jsonpath: str = "data[*]"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("title", th.StringType),
        th.Property("nickname", th.StringType),
        th.Property("href", th.StringType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Perform post processing, including queuing up any child stream types."""
        return {
            "survey_id": record["id"]
        }

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["per_page"] = self.config.get("per_page")
        params["page"]=next_page_token
        return params


class SurveyDetailsStream(SurveyMonkeyStream):
    name = "survey_details"
    path = "/surveys/{survey_id}/details"
    replication_key = 'date_modified'
    primary_keys = ["id"]
    parent_stream_type = SurveysStream
    ignore_parent_replication_keys = True
    schema_filepath = SCHEMAS_DIR / "survey_details.json"


class SurveyResponseStream(SurveyMonkeyStream):
    name = "survey_responses"
    path = "/surveys/{survey_id}/responses/bulk"
    replication_key = 'date_modified'
    primary_keys = ["id"]
    is_sorted = True
    records_jsonpath: str = "data[*]"
    parent_stream_type = SurveysStream
    ignore_parent_replication_keys = True
    schema_filepath = SCHEMAS_DIR / "survey_responses.json"

    def get_url_params(self, context: Optional[dict], next_page_token: Optional[Any]) -> Dict[str, Any]:
        """Return a dictionary of values to be used in parameterization."""
        params = {}
        if not context or "survey_id" not in context:
            raise ValueError("Cannot sync survey details without already known survey IDs.")
        params["simple"] = True
        params["per_page"] = self.config.get("per_page")
        params["sort_by"] = self.replication_key
        params["sort_order"] = "ASC"
        params["start_modified_at"] = self.starting_timestamp
        params["page"]=next_page_token
        return params


    def post_process(self, row: dict, context: Optional[dict] = None) -> dict:
        """Used to drop free text. Should move to stream maps if nested properties become supported"""
        if not self.config.get("drop_open_ended_text"):
            return row

        result = copy.copy(row)
        for page in result.get("pages", {}):
            for question in page.get("questions", {}):
                if question.get("family") == "open_ended":
                    for answer in question.get("answers"):
                        answer["text"] = None
                        answer["simple_text"] = None
        
        return result
