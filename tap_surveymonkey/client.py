"""REST client handling, including SurveyMonkeyStream base class."""

import copy
import math
from pathlib import Path
from typing import Any, Dict, Optional

from singer_sdk.authenticators import BearerTokenAuthenticator
from singer_sdk.streams import RESTStream


SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class SurveyMonkeyStream(RESTStream):
    """SurveyMonkey stream class."""

    url_base = "https://api.surveymonkey.com/v3"
    starting_timestamp = None

    @property
    def authenticator(self) -> BearerTokenAuthenticator:
        return BearerTokenAuthenticator.create_for_stream(
            self,
            token=self.config.get("access_token")
        )

    def get_next_page_token(self, response, previous_token):
        """
        Return a token for identifying next page or None if no more pages.
        The offset is the number of projects that should be skipped (not the page).
        """
        current_page = response.json().get('page')
        if not current_page:
            return
        total_pages = math.ceil(response.json().get('total')/self.config.get('per_page'))
        self.logger.debug(f"Traversed page {current_page} of {total_pages}.")
        if current_page < total_pages:
            next_page_token = current_page + 1
            self.logger.debug(f"Next page token retrieved: {next_page_token}")
            return next_page_token
        return None

    def sync(self, context: Optional[dict] = None):
        """TODO: remove when https://gitlab.com/meltano/sdk/-/issues/208 resolved"""
        self.starting_timestamp = copy.copy(self.get_starting_timestamp(context)) 
        super().sync(context)