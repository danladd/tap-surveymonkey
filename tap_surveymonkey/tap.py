"""SurveyMonkey tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th

from tap_surveymonkey.streams import (
    SurveysStream,
    SurveyDetailsStream,
    SurveyResponseStream
)

STREAM_TYPES = [
    SurveysStream,
    SurveyDetailsStream,
    SurveyResponseStream
]


class TapSurveyMonkey(Tap):
    """SurveyMonkey tap class."""
    name = "tap-surveymonkey"

    config_jsonschema = th.PropertiesList(
        th.Property("access_token", th.StringType, required=True),
        th.Property("start_date", th.DateTimeType),
        th.Property("per_page", th.IntegerType, default=100),
        th.Property("drop_open_ended_text", th.BooleanType, default=False),
        
        th.Property("stream_maps", th.ObjectType()),
        th.Property("stream_map_settings", th.ObjectType()),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]
